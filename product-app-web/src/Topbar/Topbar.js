import React from 'react'
import classes from './Topbar.module.css'

const Topbar = () => {

    return(
        <header className="App-header">
            <nav className= { classes.Topbar }>
                <img src="https://1079life.com/wp-content/uploads/2018/12/amazon_PNG11.png" />
            </nav>
        </header>
    )
}

export default Topbar;