import react, { Component } from 'react'
import ProductData from './Utils/ProductData'
import classes from './App.module.css'
import ProductPreview from './ProductPreview/ProductPreview'
import ProductDetails from './ProductDetails/ProductDetails'
import Topbar from './Topbar/Topbar'

class App extends Component {

  state = {
    productData: ProductData,
    currentPreviewImagePos: 0,
    currentSelectedFeature: 0
  }

  onColorOptionClick = (pos) => {
    this.setState({currentPreviewImagePos: pos})
  }

  onFeatureItemClick = (pos) => {
    this.setState({currentSelectedFeature: pos})
  }

  render() {
    return (
      <div className="App">
        <Topbar></Topbar>
        <div className={ classes.MainContainer }>
          <div className={ classes.ProductPreview}>
            <ProductPreview 
            currentPreviewImage={ this.state.productData.colorOptions[this.state.currentPreviewImagePos].imageUrl } 
            currentSelectedFeature={ this.state.currentSelectedFeature }>             
            </ProductPreview>
          </div> 
          <div className={ classes.ProductData }>
            <ProductDetails 
            data={ this.state.productData } 
            onColorOptionClick={ this.onColorOptionClick }
            currentPreviewImagePos={ this.state.currentPreviewImagePos }
            onFeatureItemClick={ this.onFeatureItemClick }
            currentSelectedFeature={ this.state.currentSelectedFeature }>
            </ProductDetails>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
